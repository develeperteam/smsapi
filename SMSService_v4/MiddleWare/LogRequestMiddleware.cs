﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Logging;
using Microsoft.Net.Http.Headers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SMSService_v4.MiddleWare
{
    public class LogRequestMiddleware
    {
        private readonly RequestDelegate next;

        public LogRequestMiddleware(RequestDelegate next)
        {
            this.next = next;
        }

        public async Task Invoke(HttpContext context, ILogger<LogRequestMiddleware> logger)
        {
            // string request = await ReadBodyAsync(context.Request);
            string request = GetRequestInfo(context);
            logger.LogInformation(request);

            await next(context);
        }

        private static string GetRequestInfo(HttpContext context)
        {
            StringBuilder message = new StringBuilder();
            message.Append("\r\n************************************\r\n");
            message.Append("Date：" + DateTime.Now.ToString() + "\r\n");
            message.Append("User：" + context.User.Identity.Name + "\r\n");
            message.Append("IP：" + context.Request.Host + "\r\n");
            message.Append("Browser：" + context.Request.Headers[HeaderNames.UserAgent].ToString() + "\r\n");
            message.Append("\r\n************************************\r\n");
            if (context.Request != null &&
            context.Request.HasFormContentType &&
            context.Request.Form != null &&
            context.Request.Form.Count > 0 &&
            context.Request.Form.Keys.Count > 0)
            {
                message.Append("Form Collection：" + "\r\n");
                foreach (object v in context.Request.Form.Keys)
                {
                    if (v == null)
                    {
                        continue;
                    }
                    if (v.ToString()[0] == '_')
                    {
                        continue;
                    }
                    message.Append("Form_key:" + v.ToString() + "   value: " + context.Request.Form[v.ToString()] + "\r\n"); ;
                }
                message.Append("\r\n");
            }
            message.Append("ReferrerUrl:  ");
            message.Append(context.Request.Path == null ? string.Empty : context.Request.Path + "\r\n");
            message.Append("\r\n************************************\r\n");
            return message.ToString();
        }

        #region Get Request Content

        private async Task<string> ReadBodyAsync(HttpRequest request)
        {
            if (request.ContentLength > 0)
            {
                await EnableRewindAsync(request).ConfigureAwait(false);
                var encoding = GetRequestEncoding(request);
                return await this.ReadStreamAsync(request.Body, encoding).ConfigureAwait(false);
            }
            return null;
        }

        private Encoding GetRequestEncoding(HttpRequest request)
        {
            var requestContentType = request.ContentType;
            var requestMediaType = requestContentType == null ? default(MediaType) : new MediaType(requestContentType);
            var requestEncoding = requestMediaType.Encoding;
            if (requestEncoding == null)
            {
                requestEncoding = Encoding.UTF8;
            }
            return requestEncoding;
        }

        private async Task EnableRewindAsync(HttpRequest request)
        {
            if (!request.Body.CanSeek)
            {
                request.EnableBuffering();

                await request.Body.DrainAsync(CancellationToken.None);
                request.Body.Seek(0L, SeekOrigin.Begin);
            }
        }

        private async Task<string> ReadStreamAsync(Stream stream, Encoding encoding)
        {
            using (StreamReader sr = new StreamReader(stream, encoding, true, 1024, true))//这里注意Body部分不能随StreamReader一起释放
            {
                var str = await sr.ReadToEndAsync();
                stream.Seek(0, SeekOrigin.Begin);//内容读取完成后需要将当前位置初始化，否则后面的InputFormatter会无法读取
                return str;
            }
        }

        #endregion
    }

    public static class RequestHandlingExtensions
    {
        public static IApplicationBuilder UseRequestLogHandling(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<LogRequestMiddleware>();
        }
    }
}
