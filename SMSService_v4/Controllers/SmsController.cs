﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using SMSService_v4.Interface;
using SMSService_v4.Model;

namespace SMSService_v4.Controllers
{
    [ApiController]
    [Route("[controller]/[action]")]
    public class SmsController : ControllerBase
    {
        private readonly ILogger<SmsController> _logger;
        private readonly ISmsService _smsRepository;
        private readonly TokenOptions _tokenSection;
        private readonly AliSmsOptons _alismsSection;

        public SmsController(ILogger<SmsController> logger, ISmsService smsRepository, IOptions<TokenOptions> _opts, IOptions<AliSmsOptons> _smsopts)
        {
            _logger = logger;
            _smsRepository = smsRepository;
            _tokenSection = _opts.Value;
            _alismsSection = _smsopts.Value;
        }

        //[Route("sms/send")]
        [ActionName("send")]
        [HttpPost]
        public APIResult SendSms(SmsRequest req)
        {
            var status = SendMessage(req.Mobile, req.Token, req.Template, req.TemplateParams, out string BizId);
            return new APIResult { status = status };
        }

        [ActionName("sendpwmmsg")]
        [HttpPost]
        public APIResult SendSmsForPWM([FromForm]PWMSmsRequest req)
        {

            PWMMessage message = JsonConvert.DeserializeObject<PWMMessage>(req.Message);
            string templateParam = string.IsNullOrWhiteSpace(message.Token) ? "" : JsonConvert.SerializeObject(new { Token = message.Token });
            var status = SendMessage(req.Mobile, req.Token, message.Template, templateParam, out string BizId);
            return new APIResult { status = status };         
        }

        [ActionName("sendsmstext")]
        [HttpPost]
        public APIResult SmsTextRequest(SmsTextRequest req)
        {
            var smsParams = $"{{\"{ _alismsSection.FullVarTemplateParam }\":\"{ReplaceString(req.Message)}\"}}";
            var status = SendMessage(req.Mobile, req.Token, _alismsSection.FullVarTemplateId, smsParams, out string BizId);
            return new APIResult { status = status, obj = new { BizId = BizId } };
        }


        private ResponseStatus SendMessage(string phonenumber, string smsToken, string templateCode, string templateParams, out string BizId)
        {
            BizId ="";
            if (_tokenSection == null || _tokenSection.Tokens == null || !_tokenSection.Tokens.Any(t => t.Token == smsToken))
            {
                return ResponseStatusResult.SecurityTokenError;
            }
            else if(string.IsNullOrWhiteSpace(phonenumber))
            {
                return ResponseStatusResult.NotValidPhone;
            }
            else
            {
                if(phonenumber.IndexOf(",")>-1)
                {
                    string[] phones = phonenumber.Split(",").Select(p => p.Length > 11 ? p.Substring(p.Length - 11, 11): p).ToArray();

                    if(phones.Any(p=> !FilterNumberFormatter(p)))
                    {
                        return ResponseStatusResult.NotValidPhone;
                    }
                    else
                    {
                        var token = _tokenSection.Tokens.FirstOrDefault(p => p.Token == smsToken);
                        BizId = _smsRepository.BatchSend(phones, templateCode, templateParams, token.Account, token.Password);
                        return ResponseStatusResult.OK;
                    }
                }
                else
                {
                    phonenumber = phonenumber.Length > 11 ? phonenumber.Substring(phonenumber.Length - 11, 11) : phonenumber;
                    if (!FilterNumberFormatter(phonenumber))
                    {
                        return ResponseStatusResult.NotValidPhone;
                    }
                    else
                    {
                        var token = _tokenSection.Tokens.FirstOrDefault(p => p.Token == smsToken);
                        BizId =_smsRepository.Send(phonenumber, templateCode, templateParams, token.Account, token.Password);
                        return ResponseStatusResult.OK;
                    }
                }
            }
        }
        private bool FilterNumberFormatter(string number)
        {
            Regex reg = new Regex(@"^1[3|4|5|6|7|8|9]\d{9}$");
            return reg.IsMatch(number);
        }
        private string ReplaceString(string JsonString)
        {
            if (JsonString == null) { return JsonString; }
            if (JsonString.Contains("\\"))
            {
                JsonString = JsonString.Replace("\\", "\\\\");
            }
            if (JsonString.Contains("\'"))
            {
                JsonString = JsonString.Replace("\'", "\\\'");
            }
            if (JsonString.Contains("\""))
            {
                JsonString = JsonString.Replace("\"", "\\\"");
            }
            //去掉字符串的回车换行符
            JsonString = Regex.Replace(JsonString, @"[\n\r]", "");
            JsonString = JsonString.Trim();
            return JsonString;
        }

    }
}
