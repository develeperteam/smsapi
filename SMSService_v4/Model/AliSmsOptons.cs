﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SMSService_v4.Model
{
    public class AliSmsOptons
    {
        public string Url { get; set; }
        public string AppID { get; set; }
        public string AppSecret { get; set; }
        public string TempSignature { get; set; }
        public string Version { get; set; }
        public string RegionId { get; set; }
        public string FullVarTemplateId { get; set; }
        public string FullVarTemplateParam { get; set; }
        public string TimeoutInMilliSeconds { get; set; }
    }
}
