﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SMSService_v4.Model
{

    public class SmsRequest
    {
        public string Token { get; set; }
        public string Mobile { get; set; }
        public string Template { get; set; }
        public string TemplateParams { get; set; }
    }


    public class PWMSmsRequest
    {
        public string Token { get; set; }
        public string Mobile { get; set; }
        public string Message { get; set; }
    }

    public class PWMMessage
    {
        public string Template { get; set; }
        public string Token { get; set; }
    }

    public class SmsTextRequest
    {
        public string Token { get; set; }
        public string Mobile { get; set; }
        public string Message { get; set; }
    }
}
