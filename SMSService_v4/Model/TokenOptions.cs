﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SMSService_v4.Model
{
    public class TokenOptions
    {
        public TokenOpt[] Tokens { get;set; }
    }

    public class TokenOpt
    {
        public string Token { get; set; }
        public string Account { get; set; }
        public string Password { get; set; }
    }
}
