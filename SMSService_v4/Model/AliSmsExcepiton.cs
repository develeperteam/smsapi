﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SMSService_v4.Model
{
    public class AliSmsException : ApplicationException
    {
        private string error;
        private Exception innerException;

        public AliSmsException()
        {

        }

        public AliSmsException(string msg) : base(msg)
        {
            this.error = msg;
        }

        public AliSmsException(string msg, Exception innerException) : base(msg)
        {
            this.innerException = innerException;
            this.error = msg;
        }
        public string GetError()
        {
            return error;
        }
    }
}
