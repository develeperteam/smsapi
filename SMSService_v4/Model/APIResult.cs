﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SMSService_v4.Model
{
    public class APIResult
    {
        public ResponseStatus status { get; set; }
        public object obj { get; set; }
    }

    public class ResponseStatus
    {        
        public string statusCode { get; set; }
        public string message { get; set; }
    }

    public class ResponseStatusResult
    {
        public static ResponseStatus OK = new ResponseStatus { statusCode = "200", message = "OK" };
        public static ResponseStatus Created = new ResponseStatus { statusCode = "201", message = "Created" };

        //RequestError
        public static ResponseStatus BadRequest = new ResponseStatus { statusCode = "400", message = "Bad Request" };
        public static ResponseStatus NotFound = new ResponseStatus { statusCode = "404", message = "Not Found" };
        public static ResponseStatus Forbidden = new ResponseStatus { statusCode = "403", message = "Forbidden" };
        public static ResponseStatus RequestTimeout = new ResponseStatus { statusCode = "408", message = "Request Timeout" };

        //ServerError
        public static ResponseStatus InternalServerError = new ResponseStatus { statusCode = "500", message = "Internal Server Error" };

        public static ResponseStatus Existed = new ResponseStatus { statusCode = "901", message = "Data has existed" };
        public static ResponseStatus NotExist = new ResponseStatus { statusCode = "902", message = "Data not exist" };

        public static ResponseStatus ExceedLenth = new ResponseStatus { statusCode = "903", message = "Message lenth exceed " };
        public static ResponseStatus EmptyMessage = new ResponseStatus { statusCode = "907", message = "Message is empty " };
        public static ResponseStatus NotValidPhone = new ResponseStatus { statusCode = "905", message = "Phone number is not valid" };

        public static ResponseStatus SecurityTokenError = new ResponseStatus { statusCode = "906", message = "Your security token is not correct" };

        public static ResponseStatus AliSmsError = new ResponseStatus { statusCode = "900", message = "Ali sms service return an error!" };
    }
}
