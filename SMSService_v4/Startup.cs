using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using SMSService_v4.Interface;
using SMSService_v4.MiddleWare;
using SMSService_v4.Model;
using SMSService_v4.Provider;

namespace SMSService_v4
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddOptions();
            services.Configure<AliSmsOptons>(Configuration.GetSection("AliSmsConfig"));
            services.Configure<TokenOptions>(Configuration.GetSection("APIToken"));
            services.AddScoped<ISmsService, SmsProvider>();
            services.AddControllers();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        //public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILogger<Startup> logger)
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseRequestLogHandling();
                app.UseDeveloperExceptionPage();
            }
            app.UseErrorHandling();

            //app.Use(async (context, next) =>
            //{
            //    try
            //    {
            //        await next();
            //    }
            //    catch (Exception ex)
            //    {
            //        logger.LogError(ex, "Error");
            //        string json;

            //        if (ex is AliSmsException)
            //        {

            //            json= JsonConvert.SerializeObject(new APIResult
            //            {
            //                status = ResponseStatusResult.AliSmsError
            //            });
            //        }
            //        else
            //        {
            //            json = JsonConvert.SerializeObject(new APIResult
            //            {
            //                status = ResponseStatusResult.BadRequest
            //            });
            //        }

            //        context.Response.ContentType = "application/json";
            //        context.Response.StatusCode = 200;
            //        await context.Response.WriteAsync(json);
            //    }
            //});

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();


            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
