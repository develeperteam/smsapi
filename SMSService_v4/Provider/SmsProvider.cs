﻿using Aliyun.Acs.Core;
using Aliyun.Acs.Core.Http;
using Aliyun.Acs.Core.Profile;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using SMSService_v4.Interface;
using SMSService_v4.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;
using System.Threading.Tasks;

namespace SMSService_v4.Provider
{
    public class SmsProvider : ISmsService
    {
        private readonly AliSmsOptons opts;
        private readonly TokenOpt tokenOpt;

        public SmsProvider(IOptions<AliSmsOptons> _opts, IOptions<TokenOpt> tokensOpts)
        {
            opts = _opts.Value;
            tokenOpt = tokensOpts.Value;
        }

        public string Send(string phonenumber, string template, string templateparams, string sendAccount="", string sendPwd="")
        {
            Dictionary<string, string> smsparams = new Dictionary<string, string>();
            smsparams.Add("PhoneNumbers", phonenumber);
            smsparams.Add("SignName", opts.TempSignature);
            smsparams.Add("TemplateCode", template);


            if (!string.IsNullOrWhiteSpace(templateparams))
            {
                smsparams.Add("TemplateParam", templateparams);
            }

            var appID = string.IsNullOrWhiteSpace(sendAccount) ? opts.AppID : sendAccount;
            var appSecret = string.IsNullOrWhiteSpace(sendPwd) ? opts.AppSecret : sendPwd;

            return SendSms(appID, appSecret, smsparams);
        }

        public string BatchSend(string[] phonenumbers, string template, string templateparams, string sendAccount = "", string sendPwd = "")
        {
            return Send(string.Join(',', phonenumbers), template, templateparams, sendAccount, sendPwd);
            #region Disable the use SendBatchSms, because it only use to send different sms signatures sms for serval mobile numbers
            //int count = phonenumbers.Length;
            //Dictionary<string, string> smsparams = new Dictionary<string, string>();
            //smsparams.Add("PhoneNumberJson", JsonConvert.SerializeObject(phonenumbers));
            //smsparams.Add("SignNameJson", JsonConvert.SerializeObject(Enumerable.Repeat(opts.TempSignature, count).ToArray()));
            //smsparams.Add("TemplateCode", template);


            //if (!string.IsNullOrWhiteSpace(templateparams))
            //{
            //    string paramJson = string.Join(",", Enumerable.Repeat(templateparams, count).ToArray());
            //    smsparams.Add("TemplateParamJson", $"[{paramJson}]");
            //}

            //var appID = string.IsNullOrWhiteSpace(sendAccount) ? opts.AppID : sendAccount;
            //var appSecret = string.IsNullOrWhiteSpace(sendPwd) ? opts.AppSecret : sendPwd;

            //return SendSms(appID, appSecret, smsparams, "SendBatchSms");
            #endregion
        }

        private string SendSms(string appId, string appSecret, Dictionary<string, string> paramlist, string action= "SendSms")
        {


            IClientProfile profile = DefaultProfile.GetProfile(opts.RegionId, appId, appSecret);
            DefaultAcsClient client = new DefaultAcsClient(profile);
            CommonRequest request = new CommonRequest();
            request.Method = MethodType.POST;
            request.Domain = opts.Url;
            request.Version = opts.Version;
            request.Action = action;

            if(!string.IsNullOrEmpty(opts.TimeoutInMilliSeconds))
            {
                if(int.TryParse(opts.TimeoutInMilliSeconds,out int seconds))
                {
                    request.TimeoutInMilliSeconds = seconds;

                }
            }

            foreach (var item in paramlist)
            {
                request.QueryParameters.Add(item.Key, item.Value);
            }

            CommonResponse response = client.GetCommonResponse(request);
            string result = System.Text.Encoding.Default.GetString(response.HttpResponse.Content);
            AliSmsResponse resp = JsonConvert.DeserializeObject<AliSmsResponse>(result);
            if (resp.Code == "OK")
            {
                return resp.BizId;
            }
            else
            {
                var paramstr = JsonConvert.SerializeObject(paramlist);
                throw new AliSmsException($"Ali SmS Error Code:{resp.Code}; Error Message: {resp.Message}; Params:{paramstr}");
            }
        }
    }
}
