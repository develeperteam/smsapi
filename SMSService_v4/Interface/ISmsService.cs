﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SMSService_v4.Interface
{
    public interface ISmsService
    {
        public string Send(string phonenumber, string template, string templateparams, string sendAccount, string sendPwd);

        public string BatchSend(string[] phonenumbers, string template, string templateparams, string sendAccount = "", string sendPwd = "");
    }
}
